import logo from "./logo.svg";
import "./App.css";
import BookingTicket from "./BaiTapBookingTicket/BookingTicket";

function App() {
  return (
    <div>
      <BookingTicket />
    </div>
  );
}

export default App;
