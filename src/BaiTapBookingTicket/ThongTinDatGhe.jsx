import React, { Component } from "react";
import { connect } from "react-redux";
import { handleHuyGhe, handleXacNhan } from "./redux/action/chonGheAction";

class ThongTinDatGhe extends Component {
  render() {
    return (
      <div className="mt-3">
        <table className="table" border={2}>
          <thead>
            <tr className="text-light " style={{ fontSize: "25px" }}>
              <th>Số ghế</th>
              <th>Giá</th>
              <th></th>
            </tr>
          </thead>
          <tbody className="text-warning">
            {this.props.danhSachGheDangDat.map((gheDangChon, index) => {
              return (
                <tr key={index}>
                  <td>{gheDangChon.soGhe}</td>
                  <td>{gheDangChon.gia}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleDelete(gheDangChon.soGhe);
                      }}
                      className="btn btn-danger"
                    >
                      <i className="fa fa-trash-alt"></i>
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
          <tfoot>
            <tr className="text-light">
              <td>Tổng tiền</td>
              <td>
                {this.props.danhSachGheDangDat
                  .reduce((tongTien, gheDangChon, index) => {
                    return (tongTien += gheDangChon.gia);
                  }, 0)
                  .toLocaleString()}
              </td>
              <td>
                <button
                  className="btn btn-primary"
                  onClick={() => {
                    this.props.handleConfirm();
                  }}
                >
                  Đặt vé
                </button>
              </td>
            </tr>
          </tfoot>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    danhSachGheDangDat: state.bookingReducer.danhSachGheDangDat,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleDelete: (soGhe) => {
      dispatch(handleHuyGhe(soGhe));
    },
    handleConfirm: () => {
      dispatch(handleXacNhan());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ThongTinDatGhe);
