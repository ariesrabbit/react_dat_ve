import React, { Component } from "react";
import { connect } from "react-redux";
import { handleChonGhe } from "./redux/action/chonGheAction";

class HangGhe extends Component {
  renderGhe = () => {
    return this.props.hangGhe.danhSachGhe.map((ghe, index) => {
      let cssGheDaDat = "";
      let disabled = false;
      if (ghe.daDat) {
        cssGheDaDat = "gheDuocChon";
        disabled = true;
      }

      // xét trạng thái ghế đang đặt
      let indexGheDangDat = this.props.danhSachGheDangDat.findIndex(
        (gheDangDat) => {
          return gheDangDat.soGhe == ghe.soGhe;
        }
      );
      // nếu đã có -> đổi css thành ghế đang chọn
      if (indexGheDangDat !== -1) {
        cssGheDaDat = "gheDangChon";
      }
      return (
        <button
          onClick={() => {
            this.props.handleChoose(ghe);
          }}
          disabled={disabled}
          className={`${cssGheDaDat} ghe`}
          key={index}
        >
          {ghe.soGhe}
        </button>
      );
    });
  };
  renderSoHang = () => {
    return this.props.hangGhe.danhSachGhe.map((hang, index) => {
      return (
        <button disabled={true} className="rowNumber">
          {hang.soGhe}
        </button>
      );
    });
  };
  renderHangGhe = () => {
    if (this.props.soHangGhe === 0) {
      return (
        <div>
          {this.props.hangGhe.hang}
          {this.renderSoHang()}
        </div>
      );
    } else {
      return (
        <div>
          {this.props.hangGhe.hang}
          {this.renderGhe()}
        </div>
      );
    }
  };
  render() {
    return <div className="text-light ml-3 mt-2">{this.renderHangGhe()}</div>;
  }
}

const mapStateToProps = (state) => {
  return {
    danhSachGheDangDat: state.bookingReducer.danhSachGheDangDat,
    chairDisable: state.bookingReducer.chairDisable,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleChoose: (ghe) => {
      dispatch(handleChonGhe(ghe));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(HangGhe);
