import {
  CHON_GHE,
  HUY_CHON_GHE,
  XAC_NHAN,
} from "./../constant/bookingConstant";
import { dataChair } from "./../../../data/data";
const initialState = {
  danhSachGhe: dataChair,
  danhSachGheDangDat: [],
};

export const bookingReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CHON_GHE: {
      let cloneDanhSachGheDangDat = [...state.danhSachGheDangDat];
      let index = cloneDanhSachGheDangDat.findIndex((gheDangDat) => {
        return gheDangDat.soGhe == payload.soGhe;
      });
      if (index !== -1) {
        // nếu đã chọn ghế, mà ấn thêm lần nữa thì remove nó đi
        cloneDanhSachGheDangDat.splice(index, 1);
      } else {
        // nếu chưa có thì add vào
        cloneDanhSachGheDangDat.push(payload);
      }
      state.danhSachGheDangDat = cloneDanhSachGheDangDat;
      return { ...state };
    }
    case HUY_CHON_GHE: {
      let cloneDanhSachGheDangDat = [...state.danhSachGheDangDat];
      let index = cloneDanhSachGheDangDat.findIndex((gheDangDat) => {
        return gheDangDat.soGhe == payload;
      });
      cloneDanhSachGheDangDat.splice(index, 1);
      state.danhSachGheDangDat = cloneDanhSachGheDangDat;
      return { ...state };
    }
    case XAC_NHAN: {
      let cloneDanhSachGhe = [...state.danhSachGhe];
      let cloneDanhSachGheDangDat = [...state.danhSachGheDangDat];
      // lấy ra từng hàng ghế, sau đó lấy ra tiếp từng cái ghế
      cloneDanhSachGhe.forEach((thongTinHangGhe) => {
        thongTinHangGhe.danhSachGhe.forEach((ghe) => {
          // so sánh với từng cái ghế trong danh sách ghế đang đặt, thay đổi giá trị daDat
          cloneDanhSachGheDangDat.forEach((gheDangDat) => {
            if (gheDangDat.soGhe == ghe.soGhe) {
              ghe.daDat = true;
            }
          });
        });
      });

      state.danhSachGhe = cloneDanhSachGhe;
      cloneDanhSachGheDangDat = [];
      state.danhSachGheDangDat = cloneDanhSachGheDangDat;

      return { ...state };
    }
    default:
      return { ...state };
  }
};
