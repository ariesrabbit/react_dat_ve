import { combineReducers } from "redux";
import { bookingReducer } from "./bookingReducer";

export const rootReducer_BookingTicket = combineReducers({
  bookingReducer,
});
