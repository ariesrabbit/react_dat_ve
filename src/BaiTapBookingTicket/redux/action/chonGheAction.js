import { CHON_GHE, HUY_CHON_GHE, XAC_NHAN } from "../constant/bookingConstant";
export const handleChonGhe = (ghe) => {
  return {
    type: CHON_GHE,
    payload: ghe,
  };
};

export const handleHuyGhe = (soGhe) => {
  return {
    type: HUY_CHON_GHE,
    payload: soGhe,
  };
};

export const handleXacNhan = () => {
  return {
    type: XAC_NHAN,
  };
};
