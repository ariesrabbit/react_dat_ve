import React, { Component } from "react";
import bgMovie from "../asset/bgmovie.jpg";
import "./baiTapBookingTicket.css";
import ThongTinDatGhe from "./ThongTinDatGhe";
import { dataChair } from "../data/data";
import HangGhe from "./HangGhe";

export default class BookingTicket extends Component {
  renderHangGhe = () => {
    return dataChair.map((hangGhe, index) => {
      return (
        <div key={index}>
          <HangGhe soHangGhe={index} hangGhe={hangGhe} />;
        </div>
      );
    });
  };
  render() {
    return (
      <div
        className="bookingMovie"
        style={{
          position: "fixed",
          backgroundImage: `url(${bgMovie})`,
          width: "100vw",
          height: "100vh",
          backgroundSize: "100%",
        }}
      >
        <div
          style={{
            position: "fixed",
            backgroundColor: "rgba(0,0,0,0.7)",
            width: "100%",
            height: "100%",
          }}
        >
          <div className="container-fluid">
            <div className="row">
              <div className="col-8 ">
                <div className="text-center">
                  <h2 className="text-warning">
                    Đặt vé xem phim CYPERLEARN.VN
                  </h2>
                  <div className="mt-2 text-light" style={{ fontSize: "25px" }}>
                    Màn hình
                  </div>
                  <div className="mt-1 d-flex flex-row justify-content-center">
                    <div className="screen"></div>
                  </div>
                </div>
                <div style={{ marginLeft: "75px" }}>{this.renderHangGhe()}</div>
              </div>

              <div className="col-4 text-light">
                <h3 className="text-center mt-2">Danh sách ghế bạn chọn</h3>
                <div className="mt-4">
                  <button className="gheDuocChon" disabled={true}></button>
                  <span>Ghế đã đặt</span> <br />
                  <button className="gheDangChon" disabled={true}></button>
                  <span>Ghế đang chọn</span>
                  <br />
                  <button className="ghe m-0" disabled={true}></button>
                  <span>Ghế chưa đặt</span>
                  <br />
                </div>
                <div>
                  <ThongTinDatGhe />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
